# README #

This assignment was made to be run using opt-4-j. the jar file can be moved into the opt-4-j plug-ins folder and it will show up as “NextRelease” under the problems section.  
A few parameters can be set: 

1. Budget – The cost ratio for the budget. The budget is shown in the console when the problem is run. 
2. debugParsing – whether to print out to the console when reading the file. Prints out information for each section of the file, tick this if the file isn’t being read properly. 
3. fileName – The full address of the file to be read. 
4. singleObjective – Whether to use single objective or multiple objective evaluation.
5. Weight – the weight ratio to use for single objective mode.

The Viewer and EvolutionaryAlgorithm were used with their default values, and Nsga2 was used with a “tournament” value of 20 for multi-objective runs, while the ElitismSelector was used for single objective runs.

### What is this repository for? ###

This repository is for Assignment 3 for CS457: Advanced Topics in Software Engineering
http://classes.myplace.strath.ac.uk/course/view.php?id=15899

### How do I get set up? ###
 
- Download opt-4-j
- Export project as .jar file
- Move jar file into opt-4-j plugins folder
- Open: NextRelease under "Problems", EvolutionaryAlgorithm under Optimizer, Nsga2 under Selector in Optimizer, and Viewer under Output
- Click Run.

### Who do I talk to? ###

Baldeep Sanghera, University of Strathclyde