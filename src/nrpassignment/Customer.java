package nrpassignment;

import java.util.ArrayList;
import java.util.List;

public class Customer {

	private int profit;
	
	private int numOfRequests;
	
	private List<Integer> requests;
	
	public Customer() {
		requests = new ArrayList<Integer>();
	}
	
	public Customer(int profit, int numOfRequests, List<Integer> requests) {
		this.profit = profit;
		this.numOfRequests = numOfRequests;
		this.requests = requests;
	}

	public String asString(){
		String s = ""; 
		
		s += profit;
		
		s += " " + numOfRequests;
		
		for(int i = 0; i < requests.size(); i++){
			s += " " + requests.get(i);
		}
		
		return s;
	}
	
	/**
	 * @return the profit
	 */
	public int getProfit() {
		return profit;
	}

	/**
	 * @param profit the profit to set
	 */
	public void setProfit(int profit) {
		this.profit = profit;
	}

	/**
	 * @return the numOfRequests
	 */
	public int getNumOfRequests() {
		return numOfRequests;
	}

	/**
	 * @param numOfRequests the numOfRequests to set
	 */
	public void setNumOfRequests(int numOfRequests) {
		this.numOfRequests = numOfRequests;
	}

	/**
	 * @return the requests
	 */
	public List<Integer> getRequests() {
		return requests;
	}

	/**
	 * @param requests the requests to set
	 */
	public void setRequests(List<Integer> requests) {
		this.requests = requests;
	}
	
	
}
