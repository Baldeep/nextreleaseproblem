package nrpassignment;

import java.util.List;

import org.opt4j.core.Objective.Sign;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;
import org.opt4j.core.start.Constant;

import com.google.inject.Inject;

public class NextReleaseEvaluator implements Evaluator<String> {

	private FileObject fo;

	private boolean isSingleObjective = false;
	private double weight;
	
	// Used to find single objective best result under budget.
	private double bestScore = 0;
	private double budget;

	@Inject
	public NextReleaseEvaluator(FileParser parser, 
			@Constant(value="single_objective") boolean isSingleObjective, 
			@Constant(value="weight") double weight) {
		fo = parser.parseFile();
		fo.printInfo();
		
		this.isSingleObjective = isSingleObjective;
		this.weight = weight;
		this.budget = fo.getBudget();
	}

	public Objectives evaluate(String phenotype) {
		Objectives objectives = new Objectives();
		
		if(!isSingleObjective){ // Multi-objective
			objectives.add("Cost", Sign.MIN,
					evaluateRequirementCostFitness(phenotype));
			objectives.add("Score", Sign.MAX,
					evaluateRequirementScoreFitness(phenotype));
		} else {
			// Single objective
			objectives.add("Fitness", Sign.MAX, evaluateSingleFitness(phenotype));
		}
		
		return objectives;
	}

	private double evaluateSingleFitness(String phenotype) {
		double fitness = 0.0;
		
		double cost = evaluateRequirementCostFitness(phenotype);
		double score = evaluateRequirementScoreFitness(phenotype);
		fitness = (weight * (cost)) + ((1 - weight) * score);
		
		if(cost < budget && score > bestScore){
			System.out.println("Best Score found under budget: " + score + ", cost: " + cost);
		}
		
		return fitness;
	}

	private int evaluateRequirementCostFitness(String phenotype){
		int cost = 0;
		List<Integer> costList = fo.getCostsList();

		for(int i = 0; i < phenotype.length(); i++){
			if(phenotype.charAt(i) == '1'){
				cost += costList.get(i);
			}
		}

		return cost;
	}


	public double evaluateRequirementScoreFitness(String phenotype){
		double score = 0;
		// loop through the requirements picked
		double totalWeight = fo.getTotalCosts();

		for(int i = 0; i < phenotype.length(); i++){
			// If a requirement was picked
			if(phenotype.charAt(i) == '1'){
				// loop through all customers to check their requirements
				for(Customer c : fo.getCustomers()){
					// the value of the requirement goes by the index of the requirement
					// in the customer requirement list
					if(c.getRequests().contains(i)){
						int value = c.getNumOfRequests() - c.getRequests().indexOf(i);
						score += value * (c.getProfit()/totalWeight);
					}
				}
			}
		}


		return score;
	}
}