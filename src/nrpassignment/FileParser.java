package nrpassignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.opt4j.core.start.Constant;

import com.google.inject.Inject;

public class FileParser {

	private  int lineNum = 0;

	private String fileName;
	private boolean debugParsing; 
	private double budget;

	@Inject
	public FileParser(@Constant(value="file_name") String fileName, 
			@Constant(value="debug_parsing") boolean debugParsing,
			@Constant(value="budget_cost_ratio") double budget) {
		this.fileName = fileName;
		this.debugParsing = debugParsing;
		this.budget = budget;
	}

	public FileObject parseFile(){
		long start = System.currentTimeMillis();

		FileObject file = new FileObject();

		System.out.println("READING FILE");

		file.setFileName(fileName);
		file.setBudgetRatio(budget);

		File f = new File(fileName);
		try {			
			BufferedReader br = new BufferedReader(new FileReader(f));

			String line = "";

			line = nextLine(br);

			int levels = Integer.parseInt(line);				// Read the number of levels
			file.setLevels(levels);
			if(debugParsing)
				System.out.println(levels + " levels of requirements.");

			for(int i = 0; i < levels; i++){					// Loop through each level
				line = nextLine(br);
				int numOfRequirements = Integer.parseInt(line);			// Get  the number of requirements in that level

				line = nextLine(br);
				Scanner lineScanner = new Scanner(line);
				ArrayList<Integer> costsOfRequirements = new ArrayList<Integer>();

				for(int j = 0; j < numOfRequirements; j++){				// Get the costs of those requirements in the level
					try{
						costsOfRequirements.add(Integer.parseInt(lineScanner.next()));
					} catch(NoSuchElementException e){
						System.out.println("Error on line " + lineNum + ": expected " + numOfRequirements + " found " + (j+1));
					}
				}

				lineScanner.close();
				List<List<Integer>> costs = file.getCostsByLevel();
				costs.add(costsOfRequirements);
				file.setCostsByLevel(costs);
				if(debugParsing)
					System.out.println(costsOfRequirements.size() + " requirements found on level " + (i+1));
			}

			line = nextLine(br);

			int numOfDependencies = Integer.parseInt(line); 				// Get the number of dependencies;
			file.setNumOfDependecies(numOfDependencies);
			if(debugParsing)
				System.out.println("Number of dependencies expected: " + numOfDependencies);

			HashMap<Integer, Integer> dependencies = file.getDependencies();

			for(int i = 0; i < numOfDependencies; i++){						// Get and put each dependency in a map
				line = nextLine(br);
				Scanner lineScanner = new Scanner(line);
				int first = lineScanner.nextInt();
				int second = lineScanner.nextInt();
				dependencies.put(first, second);
				lineScanner.close();
			}

			file.setDependencies(dependencies);
			if(debugParsing)
				System.out.println("Found " + dependencies.size() + " dependecies found.");

			line = nextLine(br);

			int numOfCustomers = Integer.parseInt(line);					// Get the number of customers

			file.setNumOfCustomers(numOfCustomers); 
			if(debugParsing)
				System.out.println("Number of Customers: " + numOfCustomers);

			List<Customer> customers = file.getCustomers();
			for(int i = 0; i < numOfCustomers; i++){
				line = nextLine(br);

				Scanner lineScanner = new Scanner(line);
				int profit = lineScanner.nextInt();
				int numOfRequirements = lineScanner.nextInt();
				ArrayList<Integer> requirements = new ArrayList<Integer>();
				for(int j = 0; j < numOfRequirements; j++){
					requirements.add(Integer.parseInt(lineScanner.next()));
				}
				lineScanner.close();

				Customer c = new Customer(profit, numOfRequirements, requirements);
				customers.add(c);
				if(debugParsing)
					System.out.println(c.asString());
			}
			file.setCustomers(customers);			

			List<Customer> custs = file.getCustomers();
			List<Integer> reqs = new ArrayList<Integer>();
			for(Customer c: custs){
				for(Integer i : c.getRequests()){
					if(!reqs.contains(i)){
						reqs.add(i);
					}
				}
			}
			if(debugParsing)
				System.out.println("Total Requirements: " + reqs.size());
			Collections.sort(reqs);
			for(int i = 1; i < reqs.size(); i++){
				if(Math.abs(reqs.get(i-1)-reqs.get(i)) != 1){
					if(debugParsing)
						System.out.println(reqs.get(i));
				}
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		long end = System.currentTimeMillis();
		if(debugParsing)
			System.out.println("Time taken to read file: " + (end-start) + "ms");

		return file;
	}

	private String nextLine(BufferedReader br){
		String line = "";
		try {
			line = br.readLine();

			if(line!=null){
				lineNum++;
				while(line.startsWith("^")){
					line = br.readLine();
					if(line == null){
						break;
					}
					lineNum++;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return line;
	}

	/*public static void main(String[] args){
		FileParser fp = new FileParser();
		FileObject fo = fp.parseFile();
	}*/
}

