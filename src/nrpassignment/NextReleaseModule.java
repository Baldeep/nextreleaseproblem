package nrpassignment;

import org.opt4j.core.problem.ProblemModule;
import org.opt4j.core.start.Constant;

public class NextReleaseModule extends ProblemModule {
	
	@Constant(value="file_name") 
	protected String fileName = "C:\\Users\\Baldeep\\Documents\\opt4j-3.1.4\\realistic-nrp\\nrp-e1.txt";
	
	@Constant(value="debug_parsing") 
	protected boolean debugParsing = false;
	
	@Constant(value="single_objective") 
	protected boolean singleObjective = false;
	
	@Constant(value="budget_cost_ratio") 
	protected double budget = 0.45;
	
	@Constant(value="weight") 
	protected double weight = 0.2;
	
	
	protected void config() {
		bindProblem(NextReleaseCreator.class, NextReleaseDecoder.class,
				NextReleaseEvaluator.class);
	}

	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public boolean isDebugParsing() {
		return debugParsing;
	}


	public void setDebugParsing(boolean debugParsing) {
		this.debugParsing = debugParsing;
	}


	public double getBudget() {
		return budget;
	}


	public void setBudget(double budget) {
		this.budget = budget;
	}


	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public boolean isSingleObjective() {
		return singleObjective;
	}

	public void setSingleObjective(boolean singleObjective) {
		this.singleObjective = singleObjective;
	}
}

