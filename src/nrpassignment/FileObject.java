package nrpassignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FileObject {
	
	private String fileName;

	private int levels;

	private List<List<Integer>> costsByLevel;

	private int numOfDependecies;

	private HashMap<Integer,Integer> dependencies;

	private int numOfCustomers;

	private List<Customer> customers;

	private List<Integer> costsList;
	
	private double budgetRatio;

	public FileObject() {
		costsByLevel = new ArrayList<List<Integer>>();
		dependencies = new HashMap<Integer, Integer>();
		customers = new ArrayList<Customer>();
	}

	/**
	 * @return the levels
	 */
	public int getLevels() {
		return levels;
	}

	/**
	 * @param levels the levels to set
	 */
	public void setLevels(int levels) {
		this.levels = levels;
	}

	/**
	 * @return the requirementsByLevel
	 */
	public List<List<Integer>> getCostsByLevel() {
		return costsByLevel;
	}

	/**
	 * @param requirementsByLevel the requirementsByLevel to set
	 */
	public void setCostsByLevel(List<List<Integer>> requirementsByLevel) {
		this.costsByLevel = requirementsByLevel;
	}

	/**
	 * @return the numOfDependecies
	 */
	public int getNumOfDependecies() {
		return numOfDependecies;
	}

	/**
	 * @param numOfDependecies the numOfDependecies to set
	 */
	public void setNumOfDependecies(int numOfDependecies) {
		this.numOfDependecies = numOfDependecies;
	}

	/**
	 * @return the dependencies
	 */
	public HashMap<Integer, Integer> getDependencies() {
		return dependencies;
	}

	/**
	 * @param dependencies the dependencies to set
	 */
	public void setDependencies(HashMap<Integer, Integer> dependencies) {
		this.dependencies = dependencies;
	}

	/**
	 * @return the numOfCustomers
	 */
	public int getNumOfCustomers() {
		return numOfCustomers;
	}

	/**
	 * @param numOfCustomers the numOfCustomers to set
	 */
	public void setNumOfCustomers(int numOfCustomers) {
		this.numOfCustomers = numOfCustomers;
	}

	/**
	 * @return the customers
	 */
	public List<Customer> getCustomers() {
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public List<Integer> getCostsList() {
		costsList = new ArrayList<Integer>();
		for(int i = 0; i < getCostsByLevel().size() ; i++){
			for(int j = 0; j < getCostsByLevel().get(i).size(); j++){
				costsList.add(getCostsByLevel().get(i).get(j));
			}
		}
		return costsList;
	}

	public int getTotalCosts(){
		int cost = 0;
		for(Integer i : getCostsList()){
			cost+= i;
		}
		return cost;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public double getBudgetRatio() {
		return budgetRatio;
	}

	public void setBudgetRatio(double budget) {
		this.budgetRatio = budget;
	}

	public double getBudget(){
		return getTotalCosts() * budgetRatio;
	}
	
	public void printInfo(){
		System.out.println("File Name: " + fileName);
		System.out.println("Number of Requirements: " + getCostsList().size());
		System.out.println("Total costs of requirements: " + getTotalCosts());
		System.out.println("The budget cost-ratio is " + budgetRatio);
		System.out.println("Budget: " + (getTotalCosts() * budgetRatio));
	}

}
