package nrpassignment;

import java.util.Random;

import org.opt4j.core.genotype.BooleanGenotype;
import org.opt4j.core.problem.Creator;

import com.google.inject.Inject;

public class NextReleaseCreator implements Creator<BooleanGenotype>{

	private Random random = new Random();
	
	private int genotype_len;
	
	@Inject
	public NextReleaseCreator(FileParser fp) {
		genotype_len = fp.parseFile().getCostsList().size();
	}

	public BooleanGenotype create() {
		BooleanGenotype genotype = new BooleanGenotype();
		
		genotype.init(random, genotype_len); // Better to use injection
		return genotype;
	}
}
